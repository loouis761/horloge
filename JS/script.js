function initialisationHeure() {
    var date = new Date();
    var nbDegre = 360;
    var nbSecondesPassee = ((60 * (date.getMinutes() - 1)) + date.getSeconds());

    var secondes = date.getSeconds() / 60 * nbDegre;
    var minutes = ((date.getMinutes() - 60) * (360/60) + (date.getSeconds() / 60 * (360/60))) + nbDegre;
    var heures = (date.getHours() - 12) * 30 + (nbSecondesPassee / 3600 * 30);

    console.log(date.getMinutes());

    $('.heures').css({'transform': 'rotate(' + heures + 'deg)'});
    $('.minutes').css({'transform': 'rotate(' + minutes + 'deg)'});
    $('.secondes').css({'transform': 'rotate(' + secondes + 'deg)'});

    return ((3600 - nbSecondesPassee) - 60) * 1000;
}

function changerCouleur() {
    $('body').toggleClass('modeSombre');
}

$(document).ready(function() {
    var nbSecondesRestante = initialisationHeure();
    console.log(nbSecondesRestante / 1000);

    /* Permet de lancer une alert dans refresh la page*/
    setInterval("alert(\"Il est \" + new Date().getHours() + \"h\")", nbSecondesRestante);
});
